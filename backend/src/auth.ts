import {ContextParameters} from "graphql-yoga/dist/types";
import * as jwt from "jsonwebtoken";
import {rule} from "graphql-shield";
import bcrypt from "bcrypt";
import {User} from "@prisma/client";

const _JWT_PASSWORD = 'verySecurePasswordyesyes';

export interface JWTPayload {
    user: {
        id: string,
        email: string
    }
    role: string
}

export function getAndValidateToken(ctx: ContextParameters): JWTPayload|null {
    const token = ctx.request.get('Authorization');
    let decodedToken;
    if (!token) return null;
    try {
        jwt.verify(token, _JWT_PASSWORD);
        decodedToken = jwt.decode(token) as any;
    } catch (e) {
        return null;
    }
    return decodedToken;
}

export function hashPassword(pw: string): string {
    return bcrypt.hashSync(pw, 13);
}

export function comparePassword(user: User, pw: string): boolean {
    return bcrypt.compareSync(pw, user.password);
}

export function getUser(ctx: ContextParameters) {
    const decodedToken = getAndValidateToken(ctx);
    if (!decodedToken) return null;
    return decodedToken.user.id;
}

export function getUserRole(ctx: ContextParameters) {
    const decodedToken = getAndValidateToken(ctx);
    if (!decodedToken) return null;
    return decodedToken.role;
}

export const isAuthenticated = rule({cache: 'contextual'})(
    async (parent, args, ctx) => {
        return ctx.user !== null
    }
);

export const isAdmin = rule({cache: 'contextual'})(
    async (parent, args, ctx) => {
        if (ctx.user === null) return false;
        return ctx.userRole !== null && ctx.userRole === 'admin'
    }
);

export function signJWT(payload: object): string {
    return jwt.sign(payload, _JWT_PASSWORD, {expiresIn: '7d'});
}
