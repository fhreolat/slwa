import {objectType} from "nexus";

export default objectType({
    name: 'AuthResponse',
    definition(t) {
        t.string('token');
        t.field('user', {type: 'User'});
    },
})
