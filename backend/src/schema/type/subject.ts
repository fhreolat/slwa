import {objectType} from "nexus";

export default objectType({
    name: 'Subject',
    definition(t) {
        t.model.id();
        t.model.name();
        t.model.shortName();
        t.model.color();
        t.model.teacher();
    },
})
