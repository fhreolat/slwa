import {objectType} from "nexus";

export default objectType({
    name: 'Teacher',
    definition(t) {
        t.model.id();
        t.model.email();
        t.model.certifications();
        t.model.consultationHours();
        t.model.room();
        t.model.shortName();
        t.model.subjects();
        t.model.user();
        t.model.name();
        t.model.classBoard();
    },
})
