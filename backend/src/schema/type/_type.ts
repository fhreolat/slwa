import authResponse from "./authResponse";
import user from "./user";
import teacher from "./teacher";
import subject from "./subject";

export default [user, authResponse, teacher, subject]
