import {objectType} from "nexus";

export default objectType({
    name: 'User',
    definition(t) {
        t.model.id();
        t.model.email();
        t.model.role();
        t.model.teacher();
    },
})
