import {makeSchema} from "nexus";
import * as path from "path";
import {nexusPrismaPlugin} from "nexus-prisma";
import query from "./query/_query"
import mutation from './mutation/_mutation'
import type from './type/_type'

export default makeSchema({
    typegenAutoConfig: {
        contextType: '{ prisma: PrismaClient.PrismaClient }',
        sources: [{source: '@prisma/client', alias: 'PrismaClient'}],
    },
    outputs: {
        typegen: path.join(
            __dirname,
            '../../node_modules/@types/nexus-typegen/index.d.ts',
        ),
    },
    plugins: [nexusPrismaPlugin()],
    types: [
        query,
        mutation,
        ...type,
    ],
});
