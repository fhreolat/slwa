import {ObjectDefinitionBlock} from "nexus/dist/definitions/objectType";

export default function(t: ObjectDefinitionBlock<"Query">) {
    t.field('teacherCount', {
        type: 'Int',
        args: {
            search: "String"
        },
        resolve: async (root, args, ctx) => {
            const a = await ctx.prisma.teacher.findMany({
                where: {
                    OR: [
                        {name: {contains: args.search}},
                        {shortName: {contains: args.search}}
                    ]
                }
            });
            return a.length;
        }
    });
}
