import {ObjectDefinitionBlock} from "nexus/dist/definitions/objectType";
import {getUser} from "../../auth";

export default function(t: ObjectDefinitionBlock<"Query">) {
    t.field('isAuthenticated', {
        type: 'Boolean',
        resolve: () => {
            return true;
        }
    });

    t.field('isAdmin', {
        type: 'Boolean',
        resolve: () => {
            return true;
        }
    });

    t.field('getMyUser', {
        type: 'User',
        resolve: async (query, data, ctx) => {
            const userId = getUser(ctx as any);
            return ctx.prisma.user.findOne({where: {id: userId}});
        }
    })
}
