import {queryType} from "nexus";
import auth from "./auth"
import teacherCount from "./teacherCount";

export default queryType({
    definition(t) {
        auth(t);
        teacherCount(t);

        t.crud.user();
        t.crud.users({ordering: true});
        t.crud.teacher();
        t.crud.teachers({filtering: true, pagination: true});
        t.crud.subject();
        t.crud.subjects({ordering: true});
        //t.crud.files();
        //t.crud
    },
});
