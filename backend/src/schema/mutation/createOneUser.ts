import {ObjectDefinitionBlock} from "nexus/dist/definitions/objectType";
import {hashPassword} from "../../auth";
import teacher from "../type/teacher";
import {UserCreateInput} from "@prisma/client";
import user from "../type/user";

export default function (t: ObjectDefinitionBlock<"Mutation">) {
    t.field('createOneUser', {
        type: 'User',
        args: {
            data: "UserCreateWithoutTeacherInput",
            teacherId: "String"
        },
        resolve: async (parent, {data, teacherId}, ctx) => {
            if (!data) {
                throw Error("No data");
            }

            data.password = hashPassword(data.password);
            const userData = data as UserCreateInput;

            if (teacherId && teacherId.length > 0) {
                userData.teacher = {connect: {
                    id: teacherId
                }}
            } else {
                userData.teacher = null;
            }

            return ctx.prisma.user.create({data: userData});
        }
    })
}
