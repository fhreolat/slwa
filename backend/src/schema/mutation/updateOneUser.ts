import {ObjectDefinitionBlock} from "nexus/dist/definitions/objectType";
import {hashPassword} from "../../auth";
import {UserUpdateArgs, UserUpdateInput} from "@prisma/client";

export default function (t: ObjectDefinitionBlock<"Mutation">) {
    t.field('updateOneUser', {
        type: 'User',
        args: {
            where: "UserWhereUniqueInput",
            data: "UserUpdateWithoutTeacherDataInput",
            teacherId: "String"
        },
        resolve: async (parent, {where, data, teacherId}, ctx) => {
            if (!where || !data) {
                throw new Error("no data!")
            }

            let user = await ctx.prisma.user.findOne({where});

            if (!user) throw new Error("no user found");
            if (data.password) data.password = hashPassword(data.password);

            Object.assign(user, data);
            let userData = user as UserUpdateInput;
            if (teacherId && teacherId.length > 0) {
                userData.teacher = {connect: {
                        id: teacherId
                    }}
            }

            user = await ctx.prisma.user.update({where: {id: user.id}, data: userData});
            return user;
        }
    })
}
