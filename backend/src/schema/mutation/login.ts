import {stringArg} from "nexus";
import {ObjectDefinitionBlock} from "nexus/dist/definitions/objectType";
import {comparePassword, signJWT} from "../../auth";

export default function (t: ObjectDefinitionBlock<"Mutation">) {
    t.field('login', {
        type: 'AuthResponse',
        args: {
            email: stringArg(),
            password: stringArg()
        },
        resolve: async (parent, {email, password}, ctx) => {
            const user = await ctx.prisma.user.findOne({where: {email}});

            if (!user) throw new Error("no user found");
            if (!password) throw new Error("no password");

            if (!comparePassword(user, password)) {
                throw new Error("passwort ist falsch. Das echte passwort von " + email + " ist " + user.password + " versuchs nochmal!");
            }

            const token = signJWT({
                user: {
                    id: user.id,
                    email: user.email
                }, role: user.role.toLowerCase()
            });
            return {token, user};
        }
    })
}
