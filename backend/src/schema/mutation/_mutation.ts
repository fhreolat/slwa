import {mutationType} from "nexus";
import login from "./login";
import updateOneUser from "./updateOneUser";
import createOneUser from "./createOneUser";

export default mutationType({
    definition(t) {
        t.crud.createOneUser();
        t.crud.deleteOneUser();

        t.crud.updateOneTeacher();
        t.crud.updateOneSubject();
        t.crud.createOneSubject();

        login(t);
        updateOneUser(t);
        createOneUser(t);
    },
});
