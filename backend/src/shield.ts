import {not, rule, shield} from "graphql-shield";
import {isAdmin, isAuthenticated} from "./auth";

const updateTeacher = rule({cache: 'contextual'})(
    async (parent, args, ctx) => {
        if (ctx.user === null) return false;
        return ctx.userRole !== null && ctx.userRole === 'admin'
    }
);

export const permissions = shield({
    Query: {
        isAuthenticated,
        isAdmin,
    },
    Mutation: {
        login: not(isAuthenticated),
        updateOneTeacher: updateTeacher,
        updateOneSubject: isAdmin,
        createOneSubject: isAdmin,
        createOneUser: isAdmin,
        updateOneUser: isAdmin
    },
}, {
    allowExternalErrors: true
    //TODO check if prod or dev
});
