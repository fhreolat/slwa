import 'source-map-support/register'

import {GraphQLServer} from 'graphql-yoga'
import {PrismaClient} from '@prisma/client'
import {getAndValidateToken, getUser, getUserRole} from "./auth";
import {permissions} from "./shield";
import schema from "./schema/schema";
import {devSeed} from "./seeds/development"

export let prisma: PrismaClient;
export let Server;
const env = process.env.ENVIRONMENT;

export const init = () => {
    const connectionString = process.env.POSTGRES_URL;

    prisma = new PrismaClient({debug: false, datasources: {pg: connectionString}});
    Server = new GraphQLServer({
        context: ctx => ({
            ...ctx,
            prisma,
            token: getAndValidateToken(ctx),
            user: getUser(ctx),
            userRole: getUserRole(ctx)
        }),
        schema,
        middlewares: [permissions],
    });

    (async () => {
        //istanbul ignore if
        if (env === 'development' && await prisma.user.count() === 0) {
            console.log("Seeding Dev");
            await devSeed();
            console.log("Finished Seeding Dev");
        }
    })();
};

//istanbul ignore if
if (env === 'development') {
    init();
    Server.start({port: 4000}).then(() => {
        console.log(`🚀 GraphQL service ready at http://localhost:4000`)
    });
}
