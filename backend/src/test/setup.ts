import {signJWT} from "../auth";

require('source-map-support').install();

import nanoid from "nanoid";
import {test} from "../seeds/test";
import {GraphQLClient} from "graphql-request";
const util = require('util');
const exec = util.promisify(require('child_process').exec);
import path from 'path'
import {Client} from 'pg';
import {init, prisma} from "../index";

const schema = `prisma_test_${nanoid()}`;
const connectionString = `postgresql://root:htlisnice@localhost:54320/prisma?schema=${schema}`;

let httpServer;
export let client: GraphQLClient;
export const gql = s => s.toLocaleString();
export const headers = {
        none: {},
        admin: {"Authorization": signJWT({user: {id: 1, email: 'felix@wildworld.at'}, role: 'admin'})}
};

before(async function () {
        this.timeout(100000);
        process.env.ENVIRONMENT = 'test';
        process.env.POSTGRES_URL = connectionString;
        global.process.env.POSTGRES_URL = connectionString;

        // Run the migrations to ensure our schema has the required structure
        console.log("migrating " + connectionString);
        const bin = path.join(__dirname.replace(/\\/g, "/"), '../../../node_modules/.bin/prisma2').replace(/\\/g, "/");
        await exec(`${bin} migrate up --experimental`);


        await init();

        const {Server} = require("../index");

        const port = Math.floor(Math.random() * 1000 + 4000);
        httpServer = await Server.start({port: port});
        console.log(`Started on port ${port}`);

        client = new GraphQLClient('http://localhost:' + port);

        await test();
        console.log("finished");
});

after(async function () {
        this.timeout(10000);
        const {prisma} = require("../index");

        const client = new Client({
            connectionString: connectionString
        });
        console.log("dropping " + schema);
        await client.connect();
        await client.query(`DROP SCHEMA IF EXISTS "${schema}" CASCADE`);
        await client.end();

        await httpServer.close();
        await prisma.disconnect();
        console.log("teardown complete!");
    });
