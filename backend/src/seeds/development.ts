//istanbul ignore file
import {mutualSeed} from "./mutual";

export const devSeed = async () => {
    if (process.env.ENVIRONMENT !== 'development') throw new Error("won't seed development into non development env");

    await mutualSeed();
};
