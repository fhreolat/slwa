import {prisma} from "../index";
import {hashPassword} from "../auth";
import {mutualSeed} from "./mutual";

export const test = async () => {
    if (process.env.ENVIRONMENT !== 'test') throw new Error("won't seed test into non test env");
    await mutualSeed();
};
