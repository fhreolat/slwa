import {prisma} from "../index";
import {hashPassword} from "../auth";
import faker from "faker";

export const mutualSeed = async () => {
    await prisma.user.create({data: {email: 'felix@wildworld.at', password: hashPassword('1234'), role: "ADMIN"}});
    const d = await prisma.subject.create({data: {name: 'Deutsch', shortName: 'D'}});
    const e = await prisma.subject.create({data: {name: 'Englisch', shortName: 'E'}});
    const itp2 = await prisma.subject.create({data: {name: 'Informationstechnische Projekte', shortName: 'ITP2', color: '#98765f'}});
    const hies = await prisma.teacher.create({data: {email: 'hies@htl-donaustadt.at', shortName: 'HIES', name: 'Martina Hiesinger', subjects: {connect: {id: itp2.id}}}});
    await prisma.teacher.create({data: {email: 'hube@htl-donaustadt.at', shortName: 'HUBE', name: 'Ewald Huber', subjects: {connect: {id: d.id}}}});

    for (let i = 0; i < 50; i++) {
        const name = faker.name.findName();
        const email = faker.internet.email(name);

        await prisma.teacher.create({data: {name, email, shortName: name}})
    }
};
