import {client, gql, headers} from "../src/test/setup";
import {expect} from "@hapi/code";
import {prisma} from "../src";

let testTeacherId;
before(async () => {
    const res = await client.request(gql`query {
        teachers(first:1) {id}
    }`);
    expect(res.teachers).array().length(1);
    expect(res.teachers[0].id).string();

    testTeacherId = res.teachers[0].id
});

describe('Teacher', () => {
    it('allows everyone to fetch teachers', async () => {
        client.setHeaders(headers.none);

        const query = gql`query {
            teachers {
                id
                name
                email
            }
        }`;

        const res = await client.request(query);
        expect(res).exists();
        expect(res.teachers).array();
        expect(res.teachers[0].id).string();
        const id = res.teachers[0].id;
        const teacher = await prisma.teacher.findOne({where: {id}});
        expect(res.teachers[0].email).equal(teacher?.email);
    });

    it('Allows to fetch id and details separately', async () => {
        client.setHeaders(headers.none);

        const query = gql`query {
            teachers(first: 1) {
                id
            }
        }`;
        const res = await client.request(query);
        const id = res.teachers[0].id;
        expect(id).string();

        const res2 = await client.request(gql`query($id: String) {
            teacher(where: {id: $id}) {
                id
                email
            }
        }`, {id});
        expect(res2.teacher.id).equals(id);
        const teacher = await prisma.teacher.findOne({where: {id}});
        expect(res2.teacher.email).equals(teacher?.email)
    });

    it('allows to search and get total count', async function () {
        this.timeout(3000);
        client.setHeaders(headers.none);
        const query = gql`
            query {
                teachers(where: {name: {contains: "Hies"}}) {
                    id
                }
                meta: teacherCount(search: "Hies")
            }`;

        const res = await client.request(query);
        expect(res.meta).number().equal(1);
        expect(res.teachers[0].id).string();
    });

    it('does not allow non-admins to edit teachers', async function () {
        client.setHeaders(headers.none);
        const mut = gql`mutation($testTeacherId: String) {
            updateOneTeacher(where: {id: $testTeacherId}, data: {name: "test"}){id}
        } `;
        try {
            expect(await client.request(mut, {testTeacherId})).throw()
        } catch (e) {
            const res = e.response;

            expect(res.data.updateOneTeacher).null();
            expect(res.errors).array();
            expect(res.errors[0].message).equal('Not Authorised!');
        }

    });

    it('allows admin to edit teachers', async function () {
        client.setHeaders(headers.admin);

        const mut = gql`mutation($testTeacherId: String) {
            updateOneTeacher(where: {id: $testTeacherId}, data: {name: "test"}){id}
        } `;
        const res = await client.request(mut, {testTeacherId});
        expect(res.updateOneTeacher.id).string();

        const teacher = await prisma.teacher.findOne({where: {id: res.updateOneTeacher.id}});
        expect(teacher?.name).equal("test")
    })
});
