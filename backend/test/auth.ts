import {prisma} from "../src";
import {User} from "@prisma/client";
import {client, gql, headers} from "../src/test/setup";
import {expect} from "@hapi/code";

let testUser: User;

describe("Authentication", () => {
    before(async function () {
        this.timeout(5000);
        testUser = await prisma.user.findOne({where: {email: "felix@wildworld.at"}}) as User;
        expect(testUser).not.null();
    });

    it("allows the user to login", async () => {
        client.setHeaders(headers.none);

        const query = gql`
            mutation {
                login(email: "felix@wildworld.at" password: "1234") {
                    token
                    user {
                        id
                    }
                }
            }
        `;

        const res = await client.request(query);
        expect(res.login.token).string();
        expect(res.login.user.id).string();
        expect(res.login.user.id).equal(testUser.id);
    });

    it("allows to used previously gathered token", async () => {
        client.setHeaders(headers.admin);

        const query = gql`{
            isAuthenticated
        }`;

        const res = await client.request(query);
        expect(res.isAuthenticated).true();
    });

    it("allows to ask if user is admin", async () => {
        client.setHeaders(headers.admin);
        const query = gql`{isAdmin}`;
        const res = await client.request(query);
        expect(res.isAdmin).true();
    });

    it("rejects invalid auth tokens", async () => {
        client.setHeaders({"Authorization": "yes yes very legit token"});

        try {
            expect(await client.request(gql`{isAdmin}`)).throw();
        } catch (e) {
            const res = e.response;

            expect(res.data).null();
            expect(res.errors).array();
            expect(res.errors[0].message).equal('Not Authorised!');
        }
        client.setHeaders(headers.none)
    });

    it("rejects wrong password", async () => {
        client.setHeaders(headers.none);

        const query = gql`
            mutation {
                login(email: "felix@wildworld.at" password: "nottherightone") {
                    token
                    user {
                        id
                    }
                }
            }
        `;

        expect(client.request(query)).reject("Not Authorised!")
    });
});
