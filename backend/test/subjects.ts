import {client, gql, headers} from "../src/test/setup";
import {expect} from "@hapi/code";
import {prisma} from "../src";

let testSubjectId;
before(async () => {
    const res = await (prisma.subject.findMany({first: 1}));
    expect(res).array().length(1);
    testSubjectId = res[0].id;
    expect(testSubjectId).string();
});


describe('Subjects', () => {
    it('allows everyone to fetch subjects', async () => {
        client.setHeaders(headers.none);
        const query = gql`query {subjects{id, color, shortName}}`;
        const res = await client.request(query);

        expect(res.subjects).exists().array();
        expect(res.subjects[0]).object();
        expect(res.subjects[0].id).string();
    });

    it('allows admins to edit subjects', async () => {
        client.setHeaders(headers.admin);
        const query = gql`mutation($id: String, $data: SubjectUpdateInput!) {updateOneSubject(where: {id: $id}, data: $data) {id}}`;

        const res = await client.request(query, {id: testSubjectId, data: {shortName: "Yes Yes"}});
        expect(res.updateOneSubject).object();
        expect(res.updateOneSubject.id).string();

        const sub = await prisma.subject.findOne({where: {id: testSubjectId}});
        expect(sub?.shortName === 'Yes Yes');
    });

    it('does not allow non admins to edit subjects', async () => {
        client.setHeaders(headers.none);
        const query = gql`mutation($id: String, $data: SubjectUpdateInput!) {updateOneSubject(where: {id: $id}, data: $data) {id}}`;

        try {
            expect(await client.request(query, {id: testSubjectId, data: {shortName: "Yes Yes"}})).throw();
        } catch (e) {
            const res = e.response;

            expect(res.data.updateOneSubject).null();
            expect(res.errors).array();
            expect(res.errors[0].message).equal('Not Authorised!');
        }
    });

    it('does not allow non-admins to create a new subject', async () => {
        client.setHeaders(headers.none);
        const query = gql`mutation($data: SubjectCreateInput!) {createOneSubject(data: $data) {id}}`;

        try {
            expect(await client.request(query, {data: {shortName: "Yeet", name: "Yeetr"}})).throw();
        } catch (e) {
            const res = e.response;

            expect(res.data).null();
            expect(res.errors).array();
            expect(res.errors[0].message).equal('Not Authorised!');
        }
    });

    it('allows admins to create a new subject', async () => {
        client.setHeaders(headers.admin);
        const query = gql`mutation($data: SubjectCreateInput!) {createOneSubject(data: $data) {id}}`;

        const res = await client.request(query, {data: {shortName: "Y", name: "Yeet"}});
        expect(res.createOneSubject).object();
        expect(res.createOneSubject.id).string();

        const sub = await prisma.subject.findOne({where: {id: res.createOneSubject.id}});
        expect(sub?.name).equal('Yeet');
    })
});
