import {client, gql, headers} from "../src/test/setup";
import {expect} from "@hapi/code";
import {prisma} from "../src";
import {comparePassword, hashPassword} from "../src/auth";

let testUserId;
before(async () => {
    const res = await client.request(gql`query {
        users(first:1) {id}
    }`);
    expect(res.users).array().length(1);
    expect(res.users[0].id).string();

    testUserId = res.users[0].id
});

describe('User', () => {
   it('allows admin to edit users', async function () {
        client.setHeaders(headers.admin);

        const mut = gql`mutation($testUserId: String) {
            updateOneUser(where: {id: $testUserId}, data: {email: "test@test.at", password: "yeet"}){id}
        } `;

        const res = await client.request(mut, {testUserId});
        expect(res.updateOneUser.id).string();

        const user = await prisma.user.findOne({where: {id: res.updateOneUser.id}});
        if (user === null) return expect(false).true();
        expect(user.email).equal("test@test.at");
        expect(comparePassword(user, "yeet")).true();
        expect(comparePassword(user, "nööt")).false();
    });

    it('allows admin to create users', async function () {
        client.setHeaders(headers.admin);

        const mut = gql`mutation {
            createOneUser(data: {email: "test1@test.at", password: "yeet1"}){id}
        } `;

        const res = await client.request(mut, {testUserId});
        expect(res.createOneUser.id).string();

        const user = await prisma.user.findOne({where: {id: res.createOneUser.id}});
        if (user === null) return expect(false).true();
        expect(user.email).equal("test1@test.at");
        expect(comparePassword(user, "yeet1")).true();
        expect(comparePassword(user, "nööt")).false();
    })
});
