# SLWA
## Project initiator
We wanted to try out experimental/new techniques such as GraphQL.

## Lessons learned
GraphQL is cool for big projects, where frontend and backend-devs are truly seperated. In this project however, the overhead is pretty big.

## Server Setup
You first need to download and install the following software:
- docker
- nodejs
- yarn
- npm

Use the command `docker-compose up -d && yarn && && yarn start` in the folder backend, use the command `yarn && yarn serve` in the folder frontend. The services will start and you will be able to access the website.
