import Vue from 'vue'
import VueRouter from 'vue-router'
import {vueInstance} from "@/main";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import('../views/Home.vue')
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('../views/Login.vue')
    },
    {
        path: '/teacher/:id/edit',
        name: 'EditTeacher',
        component: () => import('../views/EditTeacher.vue')
    },
    {
        path: '/teacher/:id',
        name: 'Teacher',
        component: () => import('../views/Teacher.vue')
    },
    {
        path: '/admin',
        name: 'Admin',
        component: () => import('../views/admin/Admin.vue')
    },
    {
        path: '/admin/subjects',
        name: 'Admin',
        component: () => import('../views/admin/subjects/Subjects.vue')
    },
    {
        path: '/admin/subjects/create',
        name: 'Admin',
        component: () => import('../views/admin/subjects/CreateSubject')
    },
    {
        path: '/admin/subjects/:id',
        name: 'Admin',
        component: () => import('../views/admin/subjects/EditSubject.vue')
    },
    {
        path: '/admin/users',
        name: 'Admin Users',
        component: () => import('../views/admin/users/Users')
    },
    {
        path: '/admin/users/create',
        name: 'Admin User Create',
        component: () => import('../views/admin/users/CreateUser')
    },
    {
        path: '/admin/users/:id',
        name: 'AdminUserId',
        component: () => import('../views/admin/users/EditUser')
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

function isLoggedIn() {
    if (vueInstance) return vueInstance.$store.state.loggedIn;
    return false;
}

function isAdmin() {
    if (vueInstance) return isLoggedIn() && vueInstance.$store.state.isAdmin;
    return false
}

router.beforeEach((to, from, next) => {
    if (to.path === '/login' && isLoggedIn()) {
        return next('/');
    }
    if (to.path === '/admin' && !isAdmin()) {
        return next('/')
    }
    next();
});

export default router
