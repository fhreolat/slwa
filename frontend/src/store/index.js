import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        loggedIn: false,
        email: undefined,
        userId: undefined,
        isAdmin: false
    },
    mutations: {
        setLoggedIn(state, data) {
            state.loggedIn = data;
        },
        setEmail(state, data) {
            state.email = data;
        },
        setAdmin(state, data) {
            state.isAdmin = data
        },
        setUserId(state, data) {
            state.userId = data;
        }

    },
    actions: {
    },
    modules: {
    }
})
