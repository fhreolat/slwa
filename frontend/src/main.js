import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import ApolloClient from 'apollo-boost'
import VueApollo from 'vue-apollo'
import AuthService from './services/auth'
import { InMemoryCache } from 'apollo-cache-inmemory';

Vue.config.productionTip = false;

export const apolloClient = new ApolloClient({
    uri: 'http://localhost:4000',
    cache: new InMemoryCache({
        addTypename: false
    }),
    request: (operation) => {
        operation.setContext({
            headers: {
                authorization: authService.getToken()
            }
        })
    }
});

const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
});

Vue.use(VueApollo);

export const vueInstance = new Vue({
    router,
    store,
    vuetify,
    apolloProvider,
    render: h => h(App)
}).$mount('#app');

export const authService = new AuthService();
