import {apolloClient, vueInstance} from "../main";
import isAuthenticated from "../gql/isAuthenticated.graphql"
import isAdmin from "../gql/isAdmin.graphql"
import login from "../gql/login.graphql"

export default class AuthService {
    loadPromise;

    constructor() {
        this.loadPromise = this.refresh()
    }

    async refresh() {
        await vueInstance.$apolloProvider.defaultClient.cache.reset();
        try {
            const data = (await apolloClient.query({query: isAuthenticated})).data;
            vueInstance.$store.commit('setLoggedIn', (data.isAuthenticated));
            vueInstance.$store.commit('setUserId', (data.user.id));
        } catch (e) {
            vueInstance.$store.commit('setLoggedIn', false);
            vueInstance.$store.commit('setUserId', null);
        }

        try {
            vueInstance.$store.commit('setAdmin', (await apolloClient.query({query: isAdmin})).data.isAdmin);
        } catch (e) {
            vueInstance.$store.commit('setAdmin',false);
        }
    }

    async login(email, password) {
        const res = await apolloClient.mutate({mutation: login, variables: {email, password}});
        this.setToken(res.data.login.token);
        await this.refresh();
    }

    async logout() {
        this.clearToken();
        await apolloClient.resetStore();
        await this.refresh();
        await vueInstance.$router.push('/')
    }

    setToken(token) {
        localStorage.setItem('token', token);
    }

    clearToken() {
        localStorage.removeItem('token');
    }

    getToken() {
        return localStorage.getItem('token');
    }
}
